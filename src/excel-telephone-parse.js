const ExcelJS = require("exceljs");
const { checkTelephone } = require("./utils/check-telephone");

/**
 * Получить номер телефона из excel
 * @param {string} filepath Путь к файлу excel
 * @param {number | string} woorksheetNumber Количество листов, необходимых для разбора
 * @param {number | string} columnNumber Номер столбца с номером телефона
 * @returns {string} Массив телефонных номеров
 */
async function parseTelephoneExcel(filepath, woorksheetNumber, columnNumber) {
  const workbook = new ExcelJS.Workbook();
  await workbook.xlsx.readFile(filepath);

  const worksheet = workbook.getWorksheet(woorksheetNumber);
  const telephoneCol = worksheet.getColumn(columnNumber);

  const telephones = telephoneCol.values.filter(checkTelephone);
  return telephones;
}

module.exports = {
  parseTelephoneExcel,
};
