/**
 * Получить текущую словоформу минут для предложения "в \d <минутах>"
 * @param {number} minutesNumber Считайте минуты
 * @returns Корректная словарная форма
 */
function getMinutesWordForm(minutesNumber) {
  const decimal = Math.abs(minutesNumber) % 100; 
  const lastDigit = decimal % 10;

  if (decimal > 10 && decimal < 20) return "минут";
  if (lastDigit > 1 && lastDigit < 5) return "минуты";
  if (lastDigit == 1) return "минуту";

  return "минут";
}

module.exports = {
  getMinutesWordForm,
};
