/**
 * Проверьте строку на телефонном номере
 * @param {string} telString Строка необходима для проверки
 * @returns {boolean} Дает строковый номер телефона
 */
function checkTelephone(telString) {
  return /^((8|\+?7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/.test(telString);
}

module.exports = {
  checkTelephone,
};
