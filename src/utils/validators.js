/**
 * Проверка ввода приглашения пользователей
 * @param {string} limit Пользовательский ввод
 * @returns {boolean} Допустимый или недопустимый пользовательский ввод
 */
function limitValidate(limit) {
  const limitNumber = parseInt(limit);

  if (!limitNumber) return false;
  if (limitNumber < 1 || limitNumber > 200) return false;

  return true;
}

/**
 * Проверка ввода приглашения пользователей
 * @param {string} speed Пользовательский ввод
 * @returns {boolean} Допустимый или недопустимый пользовательский ввод
 */
function speedValidate(speed) {
  const speedNumber = parseInt(speed);

  if (!speedNumber) return false;
  if (speedNumber < 1 || speedNumber > 60) return false;

  return true;
}

/**
 * Проверьте на канале exsist
 * @param {string} channelName Название канала
 * @returns {boolean} Существует или нет канал
 */
function checkExistingChannel(channelName) {
  // TODO: проверьте наличие канала
  return false;
}

module.exports = {
  limitValidate,
  speedValidate,
  checkExistingChannel,
};
